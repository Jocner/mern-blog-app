import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions de Redux
import { registrarUsuarioAction } from '../actions/registrarActions';
import { mostrarAlerta, ocultarAlertaAction } from '../actions/alertaActions';

const NuevoUsuario = ({history}) => {

    // state del componente
    const [nombre, guardarNombre] = useState('');
    const [email, guardarEmail] = useState('');

    // utilizar use dispatch y te crea una función
    const dispatch = useDispatch();

    // Acceder al state del store
    const cargando = useSelector( state => state.usuario.loading );
    const error = useSelector(state => state.usuario.error);
    const alerta = useSelector(state => state.alerta.alerta);


    // mandar llamar el action de productoAction
    const agregarUsuario = usuario => dispatch( registrarUsuarioAction(usuario) );

    // cuando el usuario haga submit
    const submitNuevoUsuario = e => {
        e.preventDefault();

        // validar formulario
        if(nombre.trim() === '' || email.trim() === '' ) {

            const alerta = {
                msg: 'Ambos campos son obligatorios',
                classes: 'alert alert-danger text-center text-uppercase p3'
            }
            dispatch( mostrarAlerta(alerta) );

            return;
        }

        // si no hay errores
        dispatch( ocultarAlertaAction() );

        // crear el nuevo producto
        agregarUsuario({
            nombre,
            email
        });

        // redireccionar
        history.push('/');
    }


    return ( 
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Registro de Usuario
                        </h2>

                        {alerta ? <p className={alerta.classes}> {alerta.msg} </p> : null }

                        <form
                            onSubmit={submitNuevoUsuario}
                        >
                            <div className="form-group">
                                <label>Nombre</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Ingrese nombre"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label>Email</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Ingrese email"
                                    name="email"
                                    value={email}
                                    onChange={e => guardarEmail(e.target.value)}
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                            >Registrar</button>
                        </form>

                        { cargando ? <p>Cargando...</p> : null }
                        
                        { error ? <p className="alert alert-danger p2 mt-4 text-center">Hubo un error</p> : null }
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default NuevoUsuario;