import React, { Fragment, useEffect } from 'react';
import Tienda from './Operaciones';

// Redux
import { useSelector, useDispatch } from 'react-redux';
import { obtenerTiendasAction } from '../actions/procesosActions';

const Productos = () => {

    const dispatch = useDispatch();

    useEffect( ()=> {

        // Consultar la api
        const cargarProductos = () => dispatch( obtenerTiendasAction() );
        cargarProductos();
        // eslint-disable-next-line
    }, []);

    // obtener el state
    const tiendas = useSelector( state => state.tiendas.tiendas );
    const error = useSelector(state => state.tiendas.error);
    const cargando = useSelector(state => state.tiendas.loading);

    return ( 
       <Fragment>
           <h2 className="text-center my-5">Listado de Tiendas y Descuentos</h2>

           { error ? <p className="font-weight-bold alert alert-danger text-center mt-4">Hubo un error</p> : null }
           
           { cargando ? <p className="text-center">Cargando....</p> : null }

           <table className="table table-striped">
               <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Acciones</th>
                    </tr>
               </thead>
               <tbody>
                   { tiendas.length === 0 ? 'No hay productos' : (
                       tiendas.map(tienda => (
                           <Tienda
                                key={tienda._id}
                                tienda={tienda}
                           />
                       ))
                   ) }
               </tbody>
           </table>
       </Fragment>
     );
}
 
export default Productos;