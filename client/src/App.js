import React from 'react';
import Header from './components/Header';
import RegistroUsuario from './components/RegistroUsuario';
import Informacion from './components/Informacion';
import NuevaTiendaDescuentoUsuario from './components/NuevaTiendaDescuentoUsuario';


import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Redux
import { Provider } from 'react-redux';
import store from './store';

function App() {
  console.log( process.env.REACT_APP_BACKEND_URL );
  return (
    <Router>
      <Provider store={store}>
          <Header />

          <div className="container mt-5">
              <Switch>
                  <Route exact path="/registrousuario" component={RegistroUsuario} />
                  <Route exact path="/" component={Informacion} />
                  <Route exact path="/tienda/nuevo" component={NuevaTiendaDescuentoUsuario} />
                

              </Switch>
          </div>
      </Provider>
    </Router>
  );
}

export default App;
