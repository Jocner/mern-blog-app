const Registro = require('../models/Registro');
const { validationResult } = require('express-validator');




const controller = {
   

     postConsulta: async(req, res) => {
      
         //revisar si hay errores
         const errores = validationResult(req);

         if( !errores.isEmpty() ) {
             return res.status(400).json({ errores: errores.array() });
         }

         //extraer data 
        let { nombre, email, tienda, monto } = req.body;
        

        try {
            
           if(!email){
            return res.status(400).json({ msg: 'Error en la comunicacion' });
           }

           let registro = await Registro.findOne({ email });

        
          console.log(registro);
           return res.status(200).json({ msg: registro });

        } catch (error) {
            console.log(error);
        }   
    }

};

module.exports = controller;