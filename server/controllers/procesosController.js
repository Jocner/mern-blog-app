const TiendaA = require('../models/TiendaA');
const Usuario = require('../models/Usuario');
const TiendaB = require('../models/TiendaB');
const TiendaC = require('../models/TiendaC');
const { validationResult } = require('express-validator');
const { findById, findOne } = require('../models/TiendaA');




const controller = {

    postNuevo: async(req, res) => {
      
        //revisar si hay errores
        let errores = validationResult(req);

        if( !errores.isEmpty() ) {
            return res.status(400).json({ errores: errores.array() });
        }

        //extraer data 
        let { nombre, email, tienda, monto } = req.body;

        let nuevoRegistro = '';        

       try {

        

        let usuario = await Usuario.findOne({email : email});
        
        let tiendas = await TiendaA.find({ tienda: tienda, usuario: usuario._id });

        console.log("valor de la tienda", tienda);

        console.log("tiendas normal", tiendas);

        for(let i = 0; i <= tiendas.length; i++) {


        if(tiendas[i] === undefined){  
           
          nuevoRegistro = new TiendaA();
          nuevoRegistro.tienda = tienda;
          nuevoRegistro.usuario = usuario._id;
          nuevoRegistro.monto = monto;

          await nuevoRegistro.save();

          return res.status(200).json({ msg: 'registrado' });

         }else{

     
          return res.status(400).json({ msg: 'ya existe registro' });
         }
   
      }   
         
       } catch (error) {
           console.log(error);
       }   
   },
   
    //Metodo Post
    postAgregar: async(req, res) => {

        //revisar si hay errores
        const errores = validationResult(req);

        if( !errores.isEmpty() ) {
            return res.status(400).json({ errores: errores.array() });
        }

        //extraer data 
        let { email, tienda, monto } = req.body;

        try {

            if (tienda ==! 'A' || tienda ==! 'B' || tienda ==! 'C' ) {
                 
               

                return res.status(200).json({ msg: 'Error en el proceso de agregar' });
           } 
            
            let usuario = await Usuario.findOne({email : email});
        
            let tiendas = await TiendaA.find({ tienda: tienda, usuario: usuario._id });

        //    let tiendaA = await TiendaA.findOne({ email });
        //    let tiendaA = await TiendaA.aggregate([
        //        {
        //            {
        //                $match: email
        //            }
        //        },

        //        {
        //            $lookup : {
        //                 from: "tiendaas",
        //                 localField: "usuario",
        //                 foreignField: "_id",
        //                 as: "tiendaa"  
        //            }
        //        }
        //    ]);

            // let tiendaA = await TiendaA.aggregate().
                
                    
            //         // match({_id : req.usuario.id}). 
                      
                
                
            //         lookup({
            //             from: "usuarios",
            //             localField: "usuario",
            //             foreignField: "_id",
            //             as: "usuario"
                           
            //         }).
                    
            //         project({
            //             usuario: {
            //                 email: 1 
            //             }
            //         });
             
            // let tiendas = await TiendaA.find();    
            
            for(let i = 0; i <= tiendas.length; i++){


           let resultado = tiendas[i].monto + monto;
                
               
                console.log(resultado);
                
             await TiendaA.updateOne( {tienda : tienda, usuario : usuario._id }, {monto : resultado});
       
          

            return res.status(200).json({ msg: 'proceso correcto' });

        }

        } catch(error){
            console.log(error);
        }

          
    },

     postDescontar: async(req, res) => {
      
         //revisar si hay errores
         const errores = validationResult(req);

         if( !errores.isEmpty() ) {
             return res.status(400).json({ errores: errores.array() });
         }

         //extraer data 
        let { email, tienda, monto } = req.body;
        
        try {
            

            let usuario = await Usuario.findOne({email : email});
        
            let tiendas = await TiendaA.find({ tienda: tienda, usuario: usuario._id });


        for(let i = 0; i <= tiendas.length; i++){    
     
            console.log("monto", monto);
            console.log("tiendas for", tiendas[i])
            console.log("monto for", tiendas[i].monto);

         //  if ( tienda) {     
                let resta =  tiendas[i].monto - monto;

                console.log(resta);
                
                await TiendaA.updateOne( {tienda : tienda, usuario : usuario._id }, {monto : resta});
         //  } 



           return res.status(200).json({ msg: 'proceso correcto 2' });
         
        }   

        } catch (error) {
            console.log(error);
        }   
    },

    postConsulta: async(req, res) => {
      
        //revisar si hay errores
        // let errores = validationResult(req);

        // if( !errores.isEmpty() ) {
        //     return res.status(400).json({ errores: errores.array() });
        // }

        //extraer data 
       let { email, tienda, monto } = req.body;
       

       try {


      // let usuario = await Usuario.findOne({email : email});
        
        let tiendas = await TiendaA.find();

        //   let tiendaA = await TiendaA.aggregate().
                
                    
        //             // match({_id : req.usuario.id}). 
                      
                
                
        //             lookup({
        //                 from: "usuarios",
        //                 localField: "usuario",
        //                 foreignField: "_id",
        //                 as: "usuario"
                           
        //             }).
                    
        //             project({
        //                 usuario: {
        //                     email: 1,
        //                     nombre: 1,
        //                     monto: 1
        //                 }
        //             });
 
     //   let usuario = await Usuario.findOne({email : email});

        // let tiendaA = await Usuario.aggregate().
                
                    
        // // match({_id : usuario._id }). 
          
    
    
        // lookup({
        //     from: "tiendaas",
        //     localField: "_id",
        //     foreignField: "usuario",
        //     as: "tiendas"
               
        // }).
        
        // project({
        //     tiendas: {
        //         usuario: 1,
        //         tienda: 1,
        //         monto: 1
        //     }
        // });

       // console.log("agregate", tiendaA);
        
         for(let i = 0; i <= tiendas.length; i++){  
        //    for(let i = 0; i <= tiendaA.length; i++){     
               
            //    for(let j = 0; j < tiendas[i].tiendas.length; j++){

        //   console.log("for", tiendaA[i]);  

        //   console.log("segundo for", tiendaA[i].tiendas[j])

          console.log("for", tiendas[i]);  

        //  console.log("segundo for", tiendas[i].tiendas[j])
        
          return res.status(200).json({ msg: tiendas });

            //    }
        }   

       } catch (error) {
           console.log(error);
       }   
   }

};

module.exports = controller;