
const express = require('express');
const router = express.Router();
//const desController = require('../controllers/descuentoController');
const desController = require('../controllers/procesosController');

const { check } = require('express-validator');


// Descontar un proceso
// api/descontar
// router.post('/', 
  
//     desController.postDescontar
// );

router.post('/', 
    [
        
        check('email', 'El email es obligatorio').isEmail(),
        check('tienda', 'La tienda es obligatoria').not().isEmpty(),
        check('monto', 'El monto es obligatorio').isNumeric()
    ],
    desController.postDescontar
);

module.exports = router;